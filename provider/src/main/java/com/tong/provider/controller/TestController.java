package com.tong.provider.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/provider")
@Api(tags = "test")
public class TestController {

    @GetMapping("/getOne")
    @ApiOperation("getOne")
    public String getOne(){
        return "qweraaaqaq";
    }
}
