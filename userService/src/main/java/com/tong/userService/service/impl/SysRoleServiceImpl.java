package com.tong.userService.service.impl;

import com.tong.userService.entity.SysRole;
import com.tong.userService.mapper.SysRoleMapper;
import com.tong.userService.service.SysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

}
