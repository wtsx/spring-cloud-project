package com.tong.userService.service.impl;

import com.tong.userService.entity.SysUsersRoles;
import com.tong.userService.mapper.SysUsersRolesMapper;
import com.tong.userService.service.SysUsersRolesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色关联 服务实现类
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
@Service
public class SysUsersRolesServiceImpl extends ServiceImpl<SysUsersRolesMapper, SysUsersRoles> implements SysUsersRolesService {

}
