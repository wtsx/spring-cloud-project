package com.tong.userService.service;

import com.tong.userService.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
public interface SysRoleService extends IService<SysRole> {

}
