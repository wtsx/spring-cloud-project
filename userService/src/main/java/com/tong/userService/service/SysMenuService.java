package com.tong.userService.service;

import com.tong.userService.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统菜单 服务类
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
public interface SysMenuService extends IService<SysMenu> {

}
