package com.tong.userService.service;

import com.tong.userService.entity.SysUsersRoles;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色关联 服务类
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
public interface SysUsersRolesService extends IService<SysUsersRoles> {

}
