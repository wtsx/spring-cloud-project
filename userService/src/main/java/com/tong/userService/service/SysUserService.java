package com.tong.userService.service;

import com.tong.userService.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * <p>
 * 系统用户 服务类
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
public interface SysUserService extends IService<SysUser> , UserDetailsService {

}
