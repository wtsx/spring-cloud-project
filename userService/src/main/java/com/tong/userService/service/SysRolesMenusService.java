package com.tong.userService.service;

import com.tong.userService.entity.SysRolesMenus;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色菜单关联 服务类
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
public interface SysRolesMenusService extends IService<SysRolesMenus> {

}
