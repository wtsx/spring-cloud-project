package com.tong.userService.service.impl;

import com.tong.userService.entity.SysRolesMenus;
import com.tong.userService.mapper.SysRolesMenusMapper;
import com.tong.userService.service.SysRolesMenusService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色菜单关联 服务实现类
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
@Service
public class SysRolesMenusServiceImpl extends ServiceImpl<SysRolesMenusMapper, SysRolesMenus> implements SysRolesMenusService {

}
