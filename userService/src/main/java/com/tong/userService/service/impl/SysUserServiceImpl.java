package com.tong.userService.service.impl;

import bean.User;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tong.userService.entity.SysUser;
import com.tong.userService.mapper.SysUserMapper;
import com.tong.userService.service.SysUserService;
import com.tong.userService.utils.RedisUtil;
import constants.RedisKeyConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import utils.ResponseUtil;

/**
 * <p>
 * 系统用户 服务实现类
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    private RedisUtil redisUtil;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Object redisObj = redisUtil.get(RedisKeyConstants.LOGIN_TOKEN_KEY + username);
        if (redisObj == null) {
            throw new UsernameNotFoundException("token超时");
        }

        User user = JSON.parseObject(redisObj.toString(), User.class);
        // 根据token获取鉴权信息
        if (user == null) {
            throw new UsernameNotFoundException("无此用户");
        }
        return user;
    }
}
