package com.tong.userService.service.impl;

import com.tong.userService.entity.SysMenu;
import com.tong.userService.mapper.SysMenuMapper;
import com.tong.userService.service.SysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统菜单 服务实现类
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

}
