package com.tong.userService.filter;

import bean.User;
import com.alibaba.fastjson.JSON;
import com.tong.userService.utils.RedisUtil;
import constants.RedisKeyConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import utils.EmptyUtil;
import utils.JwtUtils;
import utils.ResponseUtil;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class TokenFilter extends OncePerRequestFilter {


    @Autowired
    private RedisUtil redisUtil;

    @Override
    public void doFilterInternal(@NonNull HttpServletRequest request,
                                 @NonNull HttpServletResponse response,
                                 @NonNull FilterChain filterChain) throws IOException, ServletException {
        response.setContentType("application/json;charset=utf-8");
        String token = this.resolveToken(request);
        if (EmptyUtil.isNotEmpty(token)) {
            // 解析token获取userId
            String username = JwtUtils.getUsername(token);
            if (StringUtils.isBlank(username)) {
                response.getWriter().write(JSON.toJSONString(ResponseUtil.error("token超时")));
                return;
            }
            Object redisObj = redisUtil.get(RedisKeyConstants.LOGIN_TOKEN_KEY + username);
            if (redisObj == null) {
                response.getWriter().write(JSON.toJSONString(ResponseUtil.error("token超时")));
                return;
            }

            User user = JSON.parseObject(redisObj.toString(), User.class);
            // 根据token获取鉴权信息
            if (user == null) {
                response.getWriter().write(JSON.toJSONString(ResponseUtil.error("无此用户")));
                return;
            }
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user,
                    null, user.getAuthorities());
            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } else {
            response.getWriter().write(JSON.toJSONString(ResponseUtil.error("缺少token")));
            return;
        }
        filterChain.doFilter(request, response);
    }

    /**
     * 获取Token
     *
     * @param request /
     * @return /
     */
    private String resolveToken(HttpServletRequest request) {
        return request.getHeader("Authorization");
    }


    /**
     * 避免自动注册到容器的filter中
     */
    @Bean
    public FilterRegistrationBean<Filter> registration(TokenFilter filter) {
        FilterRegistrationBean<Filter> registration = new FilterRegistrationBean<>(filter);
        registration.setEnabled(false);
        return registration;
    }
}
