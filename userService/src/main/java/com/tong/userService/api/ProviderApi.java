package com.tong.userService.api;

import com.tong.userService.api.fallback.ProviderFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "provider",fallback = ProviderFallBack.class )
public interface ProviderApi {

    @GetMapping("/provider/getOne")
    public String getOne();

}
