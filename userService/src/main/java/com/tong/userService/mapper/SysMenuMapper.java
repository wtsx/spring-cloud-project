package com.tong.userService.mapper;

import com.tong.userService.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统菜单 Mapper 接口
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
