package com.tong.userService.mapper;

import com.tong.userService.entity.SysRolesMenus;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色菜单关联 Mapper 接口
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
public interface SysRolesMenusMapper extends BaseMapper<SysRolesMenus> {

}
