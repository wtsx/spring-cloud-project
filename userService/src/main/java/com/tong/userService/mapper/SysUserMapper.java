package com.tong.userService.mapper;

import com.tong.userService.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统用户 Mapper 接口
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
