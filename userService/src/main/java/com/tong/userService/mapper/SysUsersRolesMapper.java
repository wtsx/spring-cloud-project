package com.tong.userService.mapper;

import com.tong.userService.entity.SysUsersRoles;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色关联 Mapper 接口
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
public interface SysUsersRolesMapper extends BaseMapper<SysUsersRoles> {

}
