package com.tong.userService.mapper;

import com.tong.userService.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
