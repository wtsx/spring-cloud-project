package com.tong.userService.aop;

import com.alibaba.fastjson.JSON;
import com.tong.userService.utils.IpUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.UUID;

@Aspect
@Configuration
public class LogAOP {

    private static final Logger logger = LoggerFactory.getLogger(LogAOP.class);

    /**
     * 定义切点Pointcut 第一个*号：表示返回类型， *号表示所有的类型 第二个*号：表示类名，*号表示所有的类 第三个*号：表示方法名，*号表示所有的方法 后面括弧里面表示方法的参数，两个句点表示任何参数
     */
    @Pointcut("execution(*  com.tong.userService.controller..*.*(..))")
    public void executionService() {
    }

    /**
     * 方法调用之前调用
     */
    @Before(value = "executionService()")
    public void doBefore(JoinPoint joinPoint) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest();

        String username;
        String ip = IpUtils.getIpAddress(request);
        String requestId = String.valueOf(UUID.randomUUID());
        MDC.put("requestId", requestId);
        logger.info("=====>[{}]@Before：IP{}URL{}className{}参数{}",
                requestId,
                ip,
                request.getRequestURI(),
                joinPoint.getTarget().getClass().getName(),
                Arrays.toString(joinPoint.getArgs())
        );
    }

    /**
     * 方法之后调用
     *
     * @param returnValue 方法返回值
     */
    @AfterReturning(pointcut = "executionService()", returning = "returnValue")
    public void doAfterReturning(JoinPoint joinPoint, Object returnValue) {
        if (JSON.toJSONString(returnValue).length() > 1000) {
            logger.info("=====>[{}]@After：响应参数为：{}", MDC.get("requestId"),
                    JSON.toJSONString(returnValue).substring(0, 1000));
        } else {
            logger.info("=====>[{}]@After：响应参数为：{}", MDC.get("requestId"), JSON.toJSONString(returnValue));
        }

        // 处理完请求，返回内容
        MDC.clear();
    }
}