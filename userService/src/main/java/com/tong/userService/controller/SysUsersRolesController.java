package com.tong.userService.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户角色关联 前端控制器
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
@RestController
@RequestMapping("/sys-users-roles")
public class SysUsersRolesController {

}
