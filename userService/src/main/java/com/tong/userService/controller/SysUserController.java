package com.tong.userService.controller;

import bean.User;
import bean.request.LoginRequest;
import com.alibaba.fastjson.JSON;
import com.tong.userService.bean.base.ResponseUtil;
import com.tong.userService.entity.SysUser;
import com.tong.userService.service.SysUserService;
import com.tong.userService.utils.RedisUtil;
import constants.RedisKeyConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import utils.CopyUtils;
import utils.JwtUtils;

/**
 * <p>
 * 系统用户 前端控制器
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
@RestController
@RequestMapping("/sys-user")
@Api(tags = "用户接口")
public class SysUserController {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private SysUserService userService;

    /**
     * 登录
     *
     * @return
     */
    @ApiOperation(value = "登录接口")
    @PostMapping("/login")
    public ResponseUtil login(@RequestBody LoginRequest loginRequest) {
        SysUser sysUser = userService.lambdaQuery().eq(SysUser::getUsername, loginRequest.getUserName())
                .eq(SysUser::getPassword, loginRequest.getPassword())
                .last("limit 1").one();
        User resp = new User();
        CopyUtils.copyProperties(sysUser, resp, false);
        redisUtil.set(RedisKeyConstants.LOGIN_TOKEN_KEY + resp.getUsername(), JSON.toJSONString(resp),
                7 * 24 * 60 * 60 * 1000);
        String sign = JwtUtils.sign(resp.getUsername(), resp.getUserId().toString());
        return ResponseUtil.success(sign);
    }

}
