package com.tong.userService.controller;

import com.tong.userService.api.ProviderApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consumeTest")
public class TestController {

    @Qualifier("com.tong.userService.api.ProviderApi")
    @Autowired
    private ProviderApi providerApi;

    @GetMapping("getOneText")
    public String getOneText() {
      return  providerApi.getOne();
    }
}
