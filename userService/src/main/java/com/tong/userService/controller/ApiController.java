package com.tong.userService.controller;

import bean.User;
import com.tong.userService.entity.SysUser;
import com.tong.userService.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import utils.CopyUtils;

@RestController
public class ApiController {

    @Autowired
    private SysUserService sysUserService;

    @GetMapping("/getUserById")
    public User getUserById(@RequestParam("userId") String userId) {
        User resp = new User();
        SysUser byId = sysUserService.getById(userId);
        CopyUtils.copyProperties(byId, resp, false);
        return resp;
    }
}
