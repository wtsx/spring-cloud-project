package com.tong.userService.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统菜单 前端控制器
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
@RestController
@RequestMapping("/sys-menu")
public class SysMenuController {

}
