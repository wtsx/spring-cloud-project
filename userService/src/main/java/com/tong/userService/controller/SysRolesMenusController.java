package com.tong.userService.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色菜单关联 前端控制器
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
@RestController
@RequestMapping("/sys-roles-menus")
public class SysRolesMenusController {

}
