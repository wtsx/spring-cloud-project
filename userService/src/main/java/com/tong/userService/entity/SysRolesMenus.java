package com.tong.userService.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 角色菜单关联
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
@Getter
@Setter
@TableName("sys_roles_menus")
@ApiModel(value = "SysRolesMenus对象", description = "角色菜单关联")
public class SysRolesMenus implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("菜单ID")
    private Long menuId;

    @ApiModelProperty("角色ID")
    private Long roleId;


}
