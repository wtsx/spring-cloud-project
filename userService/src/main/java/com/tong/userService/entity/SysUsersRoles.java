package com.tong.userService.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户角色关联
 * </p>
 *
 * @author niutong
 * @since 2022-10-20
 */
@Getter
@Setter
@TableName("sys_users_roles")
@ApiModel(value = "SysUsersRoles对象", description = "用户角色关联")
public class SysUsersRoles implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("用户ID")
    private Long userId;

    @ApiModelProperty("角色ID")
    private Long roleId;


}
