package com.tong.userService.config;

import com.tong.userService.filter.TokenFilter;
import com.tong.userService.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.ServletException;

/**
 * @author niutong
 * 定义Spring Security配置类
 * 其中@EnableWebSecurity 表示是Spring Security的配置类
 * 其中@EnableGlobalMethodSecurity(prePostEnabled = true)表示开启@PreAuthorize、@PostAuthorize, @Secured这三个注解支持
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final TokenFilter tokenFilter;

    private final SysUserService userService;

    public WebSecurityConfig(
            SysUserService userService,
            TokenFilter tokenFilter) {
        this.userService = userService;
        this.tokenFilter = tokenFilter;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoderBean());
    }

    @Bean
    public PasswordEncoder passwordEncoderBean() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    /**
     * 配置SpringSecurity相关信息
     *
     * @param http
     * @throws Exception
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        //配置放行url
        http
                .authorizeRequests()
                // 配置不需要验证的url
                .antMatchers("/swagger-ui.html", "/swagger-resources/**",
                        "/webjars/**", "/*/api-docs", "/doc.html").permitAll()
                .antMatchers("/**/login").permitAll()
                .anyRequest().authenticated()               //配置其它url要验证
                .and()
                //关闭csrf保护
                .csrf()
                .disable()
                .sessionManagement()
                //不使用session 使用token验证
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()      //                token校验
                .addFilterBefore(tokenFilter, UsernamePasswordAuthenticationFilter.class)
        ;

    }

    /**
     * web 资源过滤器 前端->过滤器Filter->servlet->拦截器intercept->controller
     * @param web
     */
    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers(HttpMethod.OPTIONS)
                .antMatchers(
                        // static resource
                        "/",
                        "/*.html",
                        "/favicon.ico",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js",

                        // swagger
                        "/swagger-resources/**",
                        "/v2/api-docs",
                        "/webjars/**",
                        "/swagger-ui.html",
                        "/doc.html",

                        // actuator
                        "/actuator/**",
                        "/env",
                        "/**/login"
                );
    }
}