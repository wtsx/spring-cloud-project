package bean.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LoginRequest {
    @ApiModelProperty(value = "userId",example = "111")
    private String userId;
    @ApiModelProperty(value = "userName")
    private String userName;
    @ApiModelProperty(value = "password")
    private String password;
}
