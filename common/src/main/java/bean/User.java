package bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import utils.EmptyUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author zhangmy
 * @date 2021/11/25 14:15
 * @description 1、定义spring security专用用户类
 *                  必须实现UserDetails,并重写那几个方法
 *                  这个类属性的获取是通过实现UserDetailsService接口的类中的loadUserByUsername()方法获取
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User implements UserDetails, Serializable {

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * token
     */
    private String token;

    /**
     * 包含的角色
     */
    private List<RoleDto> roles;

    /**
     * 拥有的菜单权限
     */
    private List<MenuDto> menus;

    /**
     * 将用户的角色作为权限
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> auth = new ArrayList<>();
        if (EmptyUtil.isNotEmpty(roles)) {
            for (RoleDto role : roles) {
                auth.add(new SimpleGrantedAuthority(role.getName()));
            }
        }
        if (EmptyUtil.isNotEmpty(menus)){
            for (MenuDto menuDto : menus) {
                auth.add(new SimpleGrantedAuthority(menuDto.getName()));
            }
        }
        return auth;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    /**
     * 用户是否未过期
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 用户是否未锁定
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * 用户凭证是否未过期
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 用户是否启用
     * @return
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
}